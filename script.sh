
INDEX='http://pads.osp.kitchen/p/meta-grand-est-index/export/txt'
declare -i lastsnapshot=0
declare -i snapshotinterval=120
while :
do

	curl $INDEX -o index.txt
	declare -i now=$(date +"%s")
	if [ $(($now-$lastsnapshot)) -gt $snapshotinterval ]; then
		makesnapshot=1
		lastsnapshot=now
	else
		makesnapshot=0
	fi

	while IFS= read -r line
	do
		version="$(cut -d'-' -f4 <<<"$line")"
		if [ ! -d "projects/$version" ]; then
			mkdir -p projects/$version/svg
		fi
		if [ ! -d "projects/$version/snapshots" ]; then
			mkdir -p projects/$version/snapshots
		fi
		curl $line/export/txt -o projects/$version/glyphs.mp
		mpost -interaction=batchmode projects/$version/glyphs.mp
		# mpost  projects/$version/glyphs.mp
		if [ $makesnapshot = 1 ]; then
			# echo "Snapshot!"
			python svg-generator/combine.py projects/$version/svg "projects/${version}/snapshots/${now}.svg"
		fi
	done < index.txt 

	sh ./rsync.sh

	sleep 0

done



