% cheatsheet !!! https://gitlab.com/erg-type/workshop.meta-elastique/blob/master/cheatsheet/cheatsheet.md

% Table Unicode https://unicode-table.com/fr/

% ↓ Ici commence la description de nos dessins

% La ligne suivante indique à Metapost de stocker chaque figure produite dans un dossier nommé ‘svg’
outputtemplate := "projects/v1/svg/%c.svg";
% Pour plus de comodité dans la manipulation des fichiers on précise ici qu'on aimerait avoir des fichiers vectoriels, au format svg en sortie
outputformat := "svg";

def variables=
        % On définit ici la valeur de l'unité de base
        u = 40pt;
        % On peut faire varier le coefficient de cette valeur en x et en y
        ux = 1u;
        uy = 1u;

        % Les 3 lignes suivantes permettent d'afficher (1) ou d'éteindre (0) l'affichage des éléments de la grille de dessin
        grid = 1;
        hints = 1;
        dot_label = 1;

        height := 10;
        baseline := 0;
        xHeight := 4;
        ascHeight := 8;
        descHeight := -2;
        capHeight := 6;

        % L'épaisseur du trait peut être préciser en x et en y, l'inclinaison de la plume est définie avec une valeur de rotation.
        strokeX := 1u;
        strokeY := 1u;
        rotation := 0;

        % GRAISSES

        def col =
		red
        enddef;


enddef;

% Les lignes ci-dessous permettent de dessiner la grille en faisant appel aux variables définies plus haut.
def beginchar(expr keycode, width)=
        beginfig(keycode);
                variables;
                pickup pencircle scaled .2;

                draw (0 * ux, (descHeight - 2) * uy) -- 
                        (width * ux, (descHeight - 2) * uy) --
                        (width * ux, (ascHeight + 2) * uy) -- 
                        (0 * ux, (ascHeight + 2) * uy) -- 
                        cycle scaled 0 withcolor red;

                if grid = 1:
                        defaultscale := .2;
                        for i=0 upto width:
                                draw (i*ux, ascHeight*uy) -- (i*ux, descHeight*uy) withcolor .3white;
                        endfor;
                        for i=descHeight upto (ascHeight):
                                draw (width*ux, i*uy) -- (0*ux, i*uy) withcolor .3white;
                        endfor;
                fi;
                
                pickup pencircle scaled 1;

                if hints = 1:
                        % draw (0 * ux, (xHeight * uy) + os_x) -- (width * ux, (xHeight * uy) + os_x)  withcolor green;
                        % draw (0 * ux, xHeight * uy) -- (width * ux, xHeight * uy)  withcolor (green + blue);

                        draw (0 * ux, capHeight * uy) -- (width * ux, capHeight * uy)  withcolor (green + blue);
                        draw (0 * ux, xHeight * uy) -- (width * ux, xHeight * uy)  withcolor (green + blue);
                        draw (0 * ux, ascHeight * uy) -- (width * ux, ascHeight * uy)  withcolor (green + blue);
                        draw (0 * ux, descHeight * uy) -- (width * ux, descHeight * uy)  withcolor (green + blue);
                        draw (0 * ux, baseline * uy) -- (width * ux, baseline * uy)  withcolor green;
                fi;
                % linejoin := beveled;
                % linecap :=squared;
                
                pickup pencircle xscaled strokeX yscaled strokeY rotated rotation;

        enddef;

        % Pour s'y retrouver on peut matérialiser et numéroter les points du tracé.
def endchar(expr lenDots)=
        if dot_label = 1:
                defaultscale := 3;
                for i=1 upto lenDots:
                        dotlabels.urt([i]) withcolor blue;
                endfor;
        fi;
endfig;
enddef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Ici commence la description d'une première figure 


%0 - zero
beginchar(48,4);
	x1 = x2 = 0.5ux;
	x3 = x6 = 2ux;
	x4 = x5 = 3.5ux;
	y6 = 0uy;
	y1 = y5 = 1.5uy;
	y2 = y4 = 4.5uy;
	y3 = 6uy;
	draw z1--z2..z3..z4--z5..z6..cycle;
endchar(6);

%1 - THM
beginchar(49,4);
x1 = 0ux;
x2 = x3 = 2ux;
y1 = 4.5uy;
y2 = 6uy;
y3 = 0uy;
draw z1--z2--z3 withcolor col;
endchar(4);

%2 - Alexandre 
beginchar(50,3);
x5 = x1 = 0ux;
x2 = 1.5ux;
x4 = 1ux;
x6 = x3 = 3ux;
y5 = y6 = 0uy;
y4 = 2uy;
y1 = y3 = 4.5uy;
y2 = 6uy;
draw z1 .. z2 .. z3 .. z4 .. z5 -- z6 withcolor col;
endchar(6);

%3 - Hannah
beginchar(51,3);
x1 = 0ux;
x2 = x7 = 1.5ux;
x3 = 3ux;
x4 = x5 =  1ux;
x6 = 3ux;
x8 = 0ux;

y1 = 5uy;
y2 = 6uy;
y3 = 5uy;
y4 = y5 = 3.5uy;
y6 = 1uy;
y7 = 0uy;
y8 = 1uy;



draw z1 .. z2 .. z3 .. z4  withcolor col;
  
draw  z5 .. z6 .. z7 .. z8 withcolor col;


endchar(8);

%4 - Marie Dv
beginchar(52,4);
x1 =2ux;
y1 = 6uy;
x2 = 0ux;
y2 = 1uy;
x3 = 4ux;
y3 = 1uy;
x4 = 3ux;
y4 = 2uy;
x5 = 3ux;
y5 = 0uy;
draw z1 -- z2 -- z3 withcolor col;
draw z4 -- z5 withcolor col;
endchar(5);

%5 - Marie Lec
beginchar(53,4);
x1 = x3 = x4 = 0.5ux;
x2 = x5 = 3.5ux;
y1 = 0ux;
y2 = 1.75 uy;
y3 = 3.5 uy;
y4 = y5 = 6uy;
draw z1..z2..z3--z4--z5 withcolor col;
endchar(5);

%6 - Drice 
beginchar(54,3);
x1 = 2.8ux;
y1 = 5.8uy;
x2= 1.4ux;
y2= 6.1uy;
x3=0ux;
y3=4.4uy;
x4=0uy;
y4=2uy;
x5=0ux;
y5=1uy;
x6=1.5ux;
y6=-0.2uy;
x7=3ux;
y7=1uy;
x8=3ux;
y8=2uy;
x9=1.5ux;
y9=3.5uy;
path p;
p = z4--z5{down}..z6..z7--z8..z9;
path d;
d = z4..z9..z8--z7{down}..z6..z5;

pickup penrazor scaled 1.5ux rotated 88;
draw subpath(0,2.05) of p withcolor black;
draw subpath(0,4) of d withcolor black;
draw z1..z2..{down}z3--z4--z5 withcolor black;
endchar(0);

%7 - Tamar
beginchar(55,4);
x1 = x2 = 0ux;
x3 = 3ux;
x4 = 1ux;
y1 = 5uy;
y2 = y3 = 6uy;
y4 = 0uy;
draw z1--z2--z3--z4 withcolor red;
endchar(4);


%8 - Yulen
beginchar(56,4);
	x1 = x5 = 0.5ux;
	x2 = x4 = x6 = 2ux;
	x3 = x7 = 3.5ux;
	y6 = 0uy;
	y5 = y7 = 1.5uy;
	y4 = 3uy;
	y1 = y3 = 4.5uy;
	y2 = 6uy;
	
	%draw z1 .. z2...z3..z4..z5..z6..z7..z4.. cycle withcolor col;
	draw z1..z2..z3..z4..cycle withcolor col;
	draw z4..z7..z6..z5..cycle withcolor col;
	
endchar(7);

%9 - Thomas B
beginchar(57,3);
    x1 = 0.25ux;
    x7 = x8 = 0ux;
    x3 = x4 = x5 = 3ux;
    x2 = x6 = x9 = 1.5ux;
    
    y1 = 0uy;
    y2 = -0.15uy;
    y3 = 1.35uy;
    y4 = y8 = 4uy;
    y6 = 6.15uy;
    y5 = y7 = 4.65uy;
    y9 = 2.65uy;
    
    draw z1..z2{right}..tension 0..z3{up}--z5{up}..z6..{bottom}z7--z8{bottom}..z9..{bottom}z4 withcolor col;
    
endchar(9);


%A - Abigail
beginchar(65,5);

x1= 0ux;  
x2= 2.5ux;
x3= 5ux;
% x4 = 2ux;
% x5 = 4ux;

y1= y3 = 0ux;  
y2=6uy;
% y4=y5= 1.5uy;

z4 := .3[z1,z2];
z5 := .3[z3,z2];

draw z1--z2..z2--z3 withcolor col;
draw z4--z5 withcolor col;


endchar(5);

%B - jeanne Borto

beginchar(66,5);
x1=x2=x3=0ux;
x6=0ux;
x9=x4=2ux;
x7=2.5ux;
x5=x8=4ux;
y1=y9=0uy;
y8=1.5uy;
y2=y6=y7=3uy;
y5=4.5uy;
y3=y4=6uy;
draw z1--z2--z3--z4..z5..z7--z6--z7..z8..z9--z1 withcolor col;
endchar(8);


%C - Camille

beginchar(67,4);
x1 = x6 = 4ux;
x2 = x5 = 2ux;
x3 = x4 = 0ux;
y1 = 4.85uy;
y2 = 6.15uy;
y3 = 4.15uy;
y4 = 2.15uy;
y5 = -0.15uy;
y6 = 0.85uy;
draw z1..z2..z3 --z4..z5..z6 withcolor col;
pickup pensquare;

endchar(10);



%D - Dorian

beginchar(68,4);

x1=x2= 0ux;
x3=x6= 1.5ux;
x4=x5=4ux;
y2=y3= 6uy;
y4=3.5uy;
y5=2.5uy;
y1=y6= 0uy;

draw z1 -- z2 -- z3{dir 0} .. z4{dir -90} -- z5{dir -90} .. z6{dir 180}  -- z1 withcolor col;

endchar(6);

%d - dorian

beginchar(100,3);

%boucle
y1=0uy;
y2=y6=1.5uy;
y3=y5=2.5uy;
y4=4uy;
x2=x3=0ux;
x1=x4=1.5ux;
x5=x6=3ux;
draw z1{left} .. z2{up} -- z3{up} .. z4{right} .. z5{down} -- z6{down} .. z1{left} withcolor col;

%fut
x7=x8=3ux;
y7=0uy;
y8=6uy;
draw z7 -- z8 withcolor col;

endchar(8);

%E - Eugénie

beginchar(69,4);

x1=x2= 0ux;
x3=x5= 4ux;
x4= 3ux;
y2=y3= 6uy;
y1=y4=3uy;
y5= 0uy;

draw z3 -- z2 -- z1 -- z4 -- z1  -- z6 -- z5 withcolor col;
endchar(6);



%F - Léa Gastaldi

beginchar(70, 4);

x1 = x2 = x3 = 0ux;
x5 = 3ux;
x4 = 4ux;
y1 = 0uy;
y2 = y5 = 3uy;
y3 = y4 = 6uy;

pickup pencircle scaled 40;
draw z1 -- z3 -- z4 withcolor col;

pickup pencircle scaled 40;
draw z2 -- z5 withcolor col;


endchar(5);

%G - Gaby  
beginchar(71,4);
x3=x4= 0ux;
x2=x8=x5=2ux;
x1=x7=x6= 4ux;

y5= 0uy;
y6= 1.5uy;
y4= 2uy;
y8=y7= 3uy;
y3=4uy;
y1= 4.5uy;
y2= 6uy;

draw z1..z2..z3--z4..z5..z6--z7--z8 withcolor col;

endchar(6);



%H - Thomas B

beginchar(72,4);
x1 = x2 = x3 = 0ux ;
x4 = x5 = x6 = 4ux;
y1 = y6 = 0uy;
y3 = y4 = 3.2uy;
y2 = y5 = 6uy;

draw z1--z2--z3--z4--z5--z6 withcolor col;

endchar(6);

%I - Marie Lec

beginchar(73,4);
x1 = x2 = 2ux;
y1 = 0uy;
y2 = 6uy;
draw z1--z2 withcolor col;
endchar(2);

%J - Julie
beginchar(74,4);
x1 = x2 = 4ux;
x3 = 2ux;
x4 = 1ux;
y1 = 6uy;
y2 = 2uy;
y3=0uy;
y4 = 1uy;
draw z1--z2..z3..z4 withcolor col;
endchar(6);

%K - Eloïse Vo
beginchar(75,4);
x1 = x2 = x3 = 0ux;
x4 = 1.8ux;
x5 = x6 = 4ux;

y1 = y6 = 0uy;
y3 = y5 = 6uy;
y4 = 3.7uy;
y2 = 2uy;
draw z1--z2--z3 withcolor col;
draw z2--z5 withcolor col;
draw z4--z6 withcolor col;

endchar(6);

%L - Louis
beginchar(76,3);
x1 = x2 = 0ux;
x3 = 3ux;
y1 = 6uy;
y2 = y3 = 0uy;
draw z1--z2--z3 withcolor col;
endchar(3);

%M - Mathilde
beginchar(77,5);
x1 = x2 = 0ux;
x4 = x5 = 5ux;
x3 = 2.5ux;
y1 = y5 = 0uy; 
y2 = y4 = 6uy;
y3 = 1uy; 
draw z1--z2--z3--z4--z5 withcolor col;
endchar(5);

%N - Nsfiseh
beginchar(78,4);
x1 = x2 = 0ux;
x3 = x4 = 5ux;

y1 = y3 = 0uy;
y2 = y4 = 6uy;
draw z1--z2--z3--z4 withcolor col;
endchar(10);

%O - OLIVIER
beginchar(79,4);
x1 = 0ux;
x2 = 0ux;
x3 = 2ux;
x4 = 4ux;
x5 = 4ux;
x6 = 2ux;
x7 = 2ux;
x8 = 1ux; 
x9 = 2ux;
x10 = 3ux;
y1 = 2uy;
y2 = 4uy;
y3 = 6uy;
y4 = 4uy;
y5 = 2uy;
y6 = 0uy;
y7 = 2uy;
y8 = 3uy;
y9 = 4uy;
y10 = 3uy;

pickup penrazor scaled 1u rotated 30;
draw z1--z2..z3..z4--z5..z6..z1 withcolor red;
draw z7..z8..z9..z10..z7 withcolor red;
endchar(10);


%P - Pierre H
beginchar(80,4);
x1 = x6 = x2 = 0ux;
x3 = x5 = 2.5ux;
x4 = 4ux;
y1 = 0uy;
y6 = y5 = 3uy;
y4 = 4.5uy;
y3 = y2 = 6uy;
draw z1--z2--z3 withcolor col;
draw z5--z6 withcolor col;
  path arrondi;
  arrondi = z3{right}..z4..{left}z5;
  draw arrondi withcolor blue;
  draw subpath(0.5,1.5) of arrondi withcolor red;
endchar(6);


 %Q - Pierre M
beginchar(81,4);
	
	x1 = x2 = 0ux;
x3 = x6 = 2ux;
x4 = x5 = 4ux;
x7 = 2ux;
x8 = 4ux;
%x9 = 3ux;
%x10 = 5ux;
x11 = 1ux;
y1 = y5 = 2uy;
y2 = y4 = 4uy;
y3 = 6uy;
y6 = 0uy;
y7 = 1uy;
y8 =-1uy;
%y9 = 1uy;
%y10 = -1uy; 
y11 = 3uy;
path p;    p := dir(-30)--dir(90)--dir(210)--cycle;   

 % On transforme ce chemin en plume.    pen pentriangle;    pentriangle := makepen(p);    % On teste notre plume    draw (0,0)--(20, 20) withpen pentriangle scaled 3;
%pickup penrazor scaled 1ux rotated 85;
draw z1--z2..z3..z4--z5..z6.. cycle withcolor blue;
draw z7--z8 withcolor blue;
%draw z11 withcolor red;
%draw z7..z8..z9..z10..;

endchar(8);

%R - CORENTIN
beginchar(82,4);
x1 = x6 = x2 = 0ux;
x3 = x5 = x7 = 2.5ux;
x4 = x8 = 4ux;
y1 = y 8 = 0uy;
y6 = y5 = y7 = 3uy;
y4 = 4.5uy;
y3 = y2 = 6uy;
 % On transforme ce chemin en plume.    pen pentriangle;    pentriangle := makepen(p);    % On teste notre plume    draw (0,0)--(20, 20) withpen pentriangle scaled 3;
pickup penrazor scaled 1ux rotated 0;
draw z1--z2--z3..z4..z5--z6--z7--z8 withcolor blue;
endchar(8);

%S - HUGO
beginchar(83,6);
x1 = x5 = 4.5ux;
x2 = x4 = 3ux;
x3 = x7 = 1ux;
x6 = 3.5ux;
x7 = 0.5ux;
x8 = 3ux;
y1 = 5.5uy;
y2 = 6uy;
y3 = 4uy;
y4 = 3uy;
y5 = 2.5uy;
y6 = 0uy;
y7= 0.75uy;
x8 = 5yx;
draw z1..z2 .. z3 .. z4 .. z5 .. z6 .. z7 withcolor col;
                   
endchar(6);

%T - Timothée 
beginchar(84,4); 
	x1 = 0ux;
	y1 = y2 = y3 = 6uy;
	x2 = 4ux;
	x3 = x4 = 2ux;
	y4 = 0uy;
	draw z1--z2 withcolor col;
	draw z3--z4 withcolor col;
	endchar(4); 

%U - Juliette F.
beginchar(85,4);
x2 = x1 = 0ux;
x3 = 2ux;
x5 = x4 = 4ux;
y3 = 0ux;
y2 = y4 = 2uy;
y1 = y5 = 6uy;
draw z1--z2..z3..z4--z5 withcolor col;
endchar(5);

%V - Manon 
beginchar(86,4);
x1 = 0ux;
x2 =2ux;
x3=4ux;
y1=6uy;
y2=0uy;
y3=6uy;
draw z1--z2--z3;
endchar(3);

%W - Jules
beginchar(87,6);
x1 = 0.5ux;
x2 = 2.25ux;
x3 = 4ux;
x4 = 5.75ux;
x5 = 7.5ux;
y1 = y3 = y5 = 6uy;
y2 = y4 = 0.5uy;
draw z1 -- z2 -- z3 -- z4 -- z5 withcolor green;
endchar(5);

%X - Drice :)
beginchar(88,4);
x1 = x4 = 0ux;
x2 = x5 = 4ux;
x3 = 2ux;
y1 = y5 = 6uy;
y2 = y4 = 0uy;
y3 = 3uy;
draw z1 -- z2 -- z3 -- z4 -- z5 withcolor col;
endchar(5);

%Y - Yeelena
beginchar(89,5);
x1=0.5ux;
x2=x3=2.5ux;
x4=4.5ux;
y3=0uy;
y1=y4=6uy;
y2=2.35uy;
draw z1--z2--z3--z2--z4 withcolor col;
endchar(4); 

%Z - Zeste
beginchar(90,3);
x1 = 0ux;
y1 = 6uy;
x2 = 3ux;
y2 = 6uy;
x3 = 0ux;
y3 = 0uy;
x4 = 3ux;
y4 = 0uy;
draw z1--z2--z3--z4 withcolor col;
endchar(4);

%& - Pierre F
beginchar(38,6);
x1 = 5ux;
y1 = 3uy;
x2 = 3ux;
y2 = 0uy;
x3 = 0ux;
y3 = 0uy;
x4 = 0ux;
y4 = 2uy;
x5 = 2ux;
y5 = 3uy;
x6 = 3ux;
y6 = 4uy;
x7 = 0ux;
y7 = 4uy;
x8 = 1ux;
y8 = 3uy;
x9 = 6ux;
y9 = 0uy;
path p;    p := dir(-30)--dir(90)--dir(210)--cycle;   

 % On transforme ce chemin en plume.    pen pentriangle;    pentriangle := makepen(p);    % On teste notre plume    draw (0,0)--(20, 20) withpen pentriangle scaled 3;
pickup penrazor scaled 1ux rotated 30;
draw z1..z2..z3..z4..z5..z6..z7..z8..z9 withcolor black;
endchar(9);

%@ - Pierre F
beginchar(64,8);
x1 = 7ux;
y1 = -1uy;
x2 = 4ux;
y2 = -2uy;
x3 = 0ux;
y3 = 2uy;
x4 = 4ux;
y4 = 6uy;
x5 = 7ux;
y5 = 1uy;
x6 = 5ux;
y6 = 0uy;
x7 = 5ux;
y7 = 4uy;
x8 = 2ux;
y8 = 2uy;
x9 = 4ux;
y9 = 0uy;

path p;    p := dir(-30)--dir(90)--dir(210)--cycle;   

 % On transforme ce chemin en plume.    pen pentriangle;    pentriangle := makepen(p);    % On teste notre plume    draw (0,0)--(20, 20) withpen pentriangle scaled 3;
pickup penrazor scaled 1ux rotated 30;

draw z1..z2..z3..z4..z5..z6--z7..z8..z9 withcolor black;
endchar(9);

%B - Pierre F
beginchar(66,5);
x1 = 0ux;
y1 = 0uy;
x2 = 1ux;
y2 = 2uy;
x3 = 1ux;
y3 = 6uy;
x4 = 0ux;
y4 = 5uy;
x5 = 3ux;
y5 = 6uy;
x6 = 4ux;
y6 = 4uy;
x7 = 1ux;
y7 = 2uy;
x8 = 5ux;
y8 = 1uy;
x9 = 0ux;
y9 = 0uy;

path p;    p := dir(-30)--dir(90)--dir(210)--cycle;   

 % On transforme ce chemin en plume.    pen pentriangle;    pentriangle := makepen(p);    % On teste notre plume    draw (0,0)--(20, 20) withpen pentriangle scaled 3;
pickup penrazor scaled 1ux rotated 30;

draw z1..z2..z3..z4..z5..z6--z7..z8..z9 withcolor black;
endchar(9);




%a - Alexandre
beginchar (97,4.5)
x1= x9 = 0ux;
x10 = x8 = x2 = 1.5ux;
x3 = x7 = x4 = 3ux;
x5 = 3.5ux;
x6 = 4.4ux;
y10 = y5 = 0uy;
y4 = y9 = 1uy;
y6 = 0.4uy;
y8 = y7 = 2uy;
y1 = y3 = 3uy;
y2 = 4uy;
draw z7 .. z8 .. z9 .. z10 .. z7 withcolor col;
draw z1 .. z2 .. {dir 270}z3 -- z4{dir 270} ... z5 .. z6 withcolor col;
endchar(0)

%b - Eugénie
beginchar (98,3);
x1=x2=x3=x7= 0ux;
y1= 6uy;
y2=y6= 0uy;
y3=y5= 1.5uy;
x4=x6= 1.5ux;
y4= 4uy;
x5=x8= 3ux;
y7=y8= 2.5uy;

draw z1 -- z2 -- z3 -- z7{dir 90} .. z4{dir 0} .. z8{dir -90} -- z5 .. z6{dir 180} .. z3{dir 90} withcolor col;
endchar(8);

%c - Louis
beginchar(99,3);
x1 = 3ux;
x2 = 2ux;
x3 = x4 = 0ux;
x5 = 2ux;
x6 = 3ux;
y1 = 2.5uy;
y2 = 3.75uy;
y3 = 2uy;
y4 = 1uy;
y5 = 0uy;
y6 = 1uy;


% draw z1..z2..z3--z4..z5..z6 withcolor col;
 path smol;
  smol = z1..z2..z3--z4;
% draw smol withcolor red;
pickup pensquare scaled 14;
  draw subpath(0.5,3) of smol withcolor green;
  path smollow;
  smollow = z6..z5..z4--z3;
 % draw smollow withcolor red;
 
  draw subpath(0.5,3) of smollow withcolor green;
endchar(3);

%e - Eugénie
beginchar(101,3);
x1=x2= 0ux;
y6= 0uy;
y1=y5= 2uy;
y2=y4= 2.5uy;
x3=x6= 1.5ux;
y3= 4uy;
x4=x5=x7= 3ux;
y7= 0.5uy;

draw z7 .. z6{dir180} .. z1{dir80} -- z2{dir 90} .. z3{dir 0} .. z4{dir -90} -- z5 -- z1 withcolor col;
endchar(7);


%f - Marie Lec
beginchar(102,4);
x1 = x2 = 2ux;
x3 = 3ux;
x4 = 1ux;
x5 = 3ux;
y1 = 0uy;
y2 = 5uy;
y3 = 6uy;
y4 = y5 = 4uy;
draw z1--z2..z3 withcolor col;
draw z4--z5 withcolor col;
endchar(5);


%g - Gaby
beginchar(103,3);
x3 = x4 = x10 = 0ux;
x2 = x5 = x9 = 1.5ux;
x7 = x1 = x6 = x8 = 3ux;

y9 = -2uy;
y10 = -1uy;
y8 = -0.5uy;
y5 = 0uy;
y6 = y4 = 1uy;
y3 = y1 = 3uy;
y2 = y7 = 4uy;

draw z1..z2..z3..z4..z5..z6 withcolor blue;
draw z7--z8..z9..z10 withcolor blue;

endchar(10);

%h - Jeanne Borto
beginchar(104,3)
x1=x2=x3=0ux;
x4=1.5ux;
x5=x6=3ux;
y1=y6=0uy;
y2=y5=1.75uy;
y4=3uy;
y3=6uy;

draw z1--z2--z3--z2..z4..z5--z6 withcolor col;
endchar(6)

%i - Eugénie
beginchar(105,0);
x1=x2=x3= 0ux;
y1= 0uy;
y2= 4uy;
y3= 6uy;
draw z1 -- z2 withcolor col;
draw z3 withcolor col;
endchar(3);

%j - Juliette F.
beginchar (106, 3);
x4 = 1ux;
x3 = x2 = x1 = 2ux;
y4 = -2uy;
y3 = -1uy;
y2 = 4uy;
y1 = 6uy;
draw z1 withcolor col;
draw z2--z3{dir 270}...{dir 180}z4 withcolor col;
endchar(5);

%k - Thomas
beginchar(107,3);
    x1 = x2 = x3 = 0ux;
    x4 = 1.5ux;
    x5 = x6 = 3ux;
    
    y1 = 0uy;
    y2 = 0.8uy;
    y3 = 6uy;
    y4 = 2.2uy;
    y5 = 4uy;
    y6 = 0uy;
    
    draw z1--z3 withcolor col;
    draw z2--z5 withcolor col;
    draw z4--z6 withcolor col;
       
endchar(6);

%l -  Manon 
beginchar(108,1);
y1= 0ux;
y2= 6ux;
draw z1--z2; withcolor red;
endchar(2);

%m - Mathilde
beginchar(109,6);
y1 = y8 = y9 = 0uy;
y2 = 4uy;
y4 = y6 = 4.1uy;
%petit changement pour avoir des vrais arc de cercle (coordonée 2.5 plutot que 2)
y3 = y5 = y7 = 2.5uy; 
x1 = x3 = x2 = 0ux;
x8 = x5 = 3ux;
x9 = x7 = 6ux;
x4 = 1.5ux;
x6 = 4.5ux;
draw z1--z2 withcolor col;
%draw z3..z4..z5--z8 withcolor col;
%draw z5..z6..z7--z9 withcolor col;
%petit changement pour avoir les tangeantes droites (commenter si on pref les boucles par défaut)
draw z3{up}..z4{right}..z5{down}--z8 withcolor col;
draw z5{up}..z6{right}..z7{down}--z9 withcolor col;
endchar(9);

%n - VOIR POUR HARMONISER AVEC m ? ^ -> fait :) 
% j'avais commencé ce glyphe
% voici mon code:
% haha on pouvais garder les deux
%oui en effet
 
beginchar(110,3);
x1=x2=0ux;
y1=0uy;
y2=4uy;
draw z1 -- z2 withcolor col;
x3 = 0ux;
x4 = 1.5ux;
x5=x6= 3ux;
y3=y5= 2.5uy;
y4= 4uy;
y6 = 0uy;
draw z3{up} .. z4{right} .. z5{down} -- z6 withcolor col;
endchar(6);

%o - Thomas
beginchar(111,3);
    x1 = x4 = 1.5ux;
    x2 = x3 = 3ux;
    x5 = x6 = 0ux;
    y1 = 0uy;
    y2 = y6 = 1.5uy;
    y3 = y5 = 2.5uy;
    y4 = 4uy;
    draw z1..z2--z3{up}..z4..{bottom}z5--z6..cycle withcolor col;
endchar(4);

%p - Oceane
beginchar(112,4);
x1 = x2 = x3 = 0ux;
x4 = x6 = 2ux;
x5 = 4ux;
y1 = -2uy;
y4 = 0uy;
y3 = y5 = 2uy;
y2 = y6 = 4uy;
draw z1--z2 withcolor col;
draw z3--z4..{up}z5..z6--z3 withcolor col;
endchar(6);

%q - Julie - Ca ne fonctionne pas :( 
beginchar(113,4);
x1 = x2 = x3 = x8 = 4ux;
x4 = x7 = 2ux;
x5 = x6 = 0ux;
y1 = -5uy;
y2 = -2uy;
y3 = y5 = 3uy;
y4 = 5uy;
y7 = 0uy;
y6 = y8 = 2uy;
draw z1 -- z2 -- z3 .. z4 . .z5 -- z6 .. z7.. z8 -- z3 withcolor col;
enchar(7);

%r -CORENTIN
beginchar(114,4);
x1 = x2 = x3 = 0ux;
x4 = 1ux;
x5 = 2ux;
y1 = y4 = 4uy;
y2 = 0uy;
y3 = y5 = 3uy;
draw z1--z2--z3{up}..z4{right}..z5{down} withcolor blue;
endchar(5);

%s - HUGO
beginchar(115,4);
x1 = 2ux;
x2 = 2ux;
x3 = 0ux;
x4 = 0ux;
x5 = 2ux;
x6 = 4ux;
x7 = 4ux;
x8 = 3ux;
y1 = 8uy;
y2 = 6uy;
y3 = 4uy;
y4 = 2uy;
y5 = 0uy;
y6 = 2uy;
y7 = 4uy;
y8 = 5uy;

x9 = 2ux;
x10 = 2ux;
x11 = 4ux;
x12 = 4ux;
x13 = 2ux;
x14 = 0ux;
x15 = 0ux;
x16 = 1ux;
y9 = 2uy;
y10 = 4uy;
y11 = 6uy;
y12 = 8uy;
y13 = 10uy;
y14 = 8uy;
y15 = 6uy;
y16 = 5uy;
draw z1--z2--z3--z4--z5--z6--z7--z8 withcolor red;
draw z9--z10--z11--z12--z13--z14--z15--z16 withcolor red;
endchar(16);

% t - Hannah
beginchar(116,4);
x1 = x2 = 1.5ux;
x3 = 2.5ux;
x4 = 0.75ux;
x5 = 2.5ux;
y1= 6uy;
y2 = 1.5uy;
y3 = 0.5uy;
y4 = y5 = 4uy;
draw z1--z2{down}...{right}z3 withcolor red;
draw z4--z5;
endchar(5);

%u - Zeste
beginchar(117,3);
x1 = 0ux;
y1 = 4uy;
x2 = 0ux;
y2 = 1.5uy;
x3 = 1.5ux;
y3 = 0uy;
x4 = 3ux;
y4 = 1.5uy;
x5 = 3ux;
y5= 4uy;
draw z1--z2..z3..z4--z5 withcolor blue;
endchar(5);

%v - Manon 
beginchar (118,4);
x1 = 0ux;
x2 =2ux;
x3=4ux;
y1=4uy;
y2=0uy;
y3=4uy;
draw z1--z2--z3 withcolor white;
endchar(3);

%w - Oceane
beginchar (119,6);
x1 = 0ux;
x2 =1.5ux;
x3 = 3ux;
x4 = 4.5ux;
x5 = 6ux;
y1 = y3 = y5 = 4uy;
y2 = y4 = 0uy;
draw z1..z2--z3--z4..z5.. withcolor col;
endchar(5);

%x- THM
beginchar (120,4);
x1 = x3 = 0ux;
x2 = x4 = 3ux;
y1 = y4 = 0uy;
y2 = y3= 4uy;
draw z1--z2 withcolor blue;
draw z3--z4 withcolor red;
endchar(3);

%y - Zeste
beginchar (121,3);
x1 = 0ux;
y1 = 4uy;
x2 = 1.5ux;
y2 = 0uy;
x3 = 3ux;
y3 = 4uy;
x4 = 1.2ux;
y4 = -1uy;
x5 = 0ux;
y5 = -2uy;
draw z1--z2 withcolor blue;
draw z3--z2..z4..z5 withcolor blue;
endchar(5);

%z - Yeelena
beginchar(122,4);
x1=0.75ux;
x3=0.5ux;
x4=x2=3.5ux;
y1=y2=4uy;
y3=y4=0uy;
draw z1--z2--z3--z4 withcolor col;
draw z3--z4 withcolor col;
endchar(4);


% ß - Yulen
beginchar(223,5);
x1 = x2 = 0ux;
x3 = 1ux;
x4 = x12 = x7 = 4ux;
x5 = x6 = x9 = x10 = 5ux;
x8 = 2.5ux;
y1 = y11 = y12 = 0uy;
y10 = 1uy;
y9 = 2uy;
y7 = y8 = 3uy;
y6 = 4uy;
y5 = y2 = 5uy;
y3 = y4 = 6uy;

draw z1--z2{up}..{right}z3--z4{right}..{down}z5--z6{down}..{left}z7--z8 withcolor col;
draw z8--z7{right}..{down}z9--z10{down}..{left}z12;

endchar(12);

% (!) - Abigail

beginchar(33,3);

x1 = x2 = x3 = 1.5ux;
y1 = 6uy;
y2= 2uy;
y3 = 0uy;

draw z1..z2;
draw z3 withcolor col;

endchar(3);

% (?) Eloïse 

beginchar(63,3);
x1 = 0ux;
x2 =x6 = x7 = 1.5ux;
x3 = 3ux;
x5= 1.7ux;
x4=2.5ux;

y1=4.8uy;
y3=5uy;
y2=6uy;
y7=0uy;
y4=3.2uy;
y5=2.6uy;
y6=2uy;

draw z1..z2..z3..z4..z5..z6 withcolor col ;
draw z7 withcolor col;


endchar(7);

%% Tamar
beginchar(37,7);
x1 = x6 = 1ux;
x2 = x5 = 0.5ux;
x3 = x4 = 0;
x7 = x10 = x11 = 1.5ux;
x8 = x9 = 2ux;
x12 = x14 = x17 = 5.5ux;
x13 = x18 = 6ux;
x15 = x16 = 5ux;
x19 = x22 = 6.5ux;
x20 = x21 = 7ux;
y1 = 2.5uy;
y2 = y10 = 2.65uy;
y3 = y9 = 3uy;
y4 = y8 = 3.5uy;
y5 = y7 = y12 = 3.85uy;
y6 = 4uy;
y13 = 0uy;
y14 = y11 = y22 = 0.15uy;
y15 = y21 = 0.65uy;
y16 = y20 = 1.15uy;
y17 = y19 = 1.35uy;
y18 = 1.5uy;
draw z1..z2..z3--z4..z5..z6..z7..z8--z9..z10..cycle;
draw z11--z12 withcolor col;
draw z13..z14..z15--z16..z17..z18..z19..z20--z21..z22..z13..cycle;
endchar(22);




end;







