# Construct new SVG
# Viewbox

# Loop through all glyphs in folder
# Open SVG
# Extract viewbox
#   get character height
#   get character width
  
# Set scale based on desired row height

# Make a group, insert elements of the glyph in it.
# Add group to output drawing
# Scale & translate group to correct position

import lxml.etree as et
import glob
import os
import os.path
import sys

glyphFolder = os.path.join(os.getcwd(), sys.argv[1]) if len(sys.argv) > 1 else None
outputFile = sys.argv[2] if len(sys.argv) > 2 else None
rowHeight = int(sys.argv[3]) if len(sys.argv) > 3 else 200
outputWidth = int(sys.argv[4]) if len(sys.argv) > 4 else 1000

if not (glyphFolder and outputFile and os.path.isdir(glyphFolder)):
  print("Usage: combine.py glyphFolder output [rowheight] [rowwidth]")
  exit(1)

outputHeight = rowHeight

x = 0
y = 0

outputTemplate = """<?xml version="1.0"?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
</svg>"""

outputRoot = et.fromstring(outputTemplate)
output = et.ElementTree(outputRoot)

# Tansform filenames back to integer again and sort
sortedGlyphFiles = sorted(os.listdir(glyphFolder), key=lambda f: int(os.path.splitext(f)[0]))

for glyphFile in sortedGlyphFiles:
  with open(os.path.join(glyphFolder, glyphFile), 'r') as h:
    glyph = et.parse(h)
    root = glyph.getroot()

    if 'viewBox' in root.attrib:
      viewBox = root.attrib['viewBox']
      xMin, yMin, xMax, yMax = map(float, viewBox.split(' '))

      width = xMax - xMin
      height = yMax - yMin

      scale = rowHeight / height

      newHeight = height * scale
      newWidth = width * scale

      if x + newWidth > outputWidth:
        x = 0
        y += rowHeight
        outputHeight += rowHeight

      g = et.SubElement(outputRoot, 'g')
      g.extend(root.iterchildren())

      g.attrib['transform'] = 'translate({0}, {1}) scale({2}, {2})'.format(x, y, scale)

      x += newWidth

outputRoot.attrib['width'] = str(outputWidth)
outputRoot.attrib['height'] = str(outputHeight)
outputRoot.attrib['viewBox'] = '0 0 {} {}'.format(outputWidth, outputHeight)

output.write(outputFile)