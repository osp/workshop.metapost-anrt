# Stroke fonts
## Workshop Metapost ANRT 

### 6, 7 & 8 November 2020

Proposed by Open Source Publishing (Brussels) and l'ANRT (Nancy)
With Antoine Gelgon, Gijs de Heij , Pierre Huyghebaert (OSP) and Thomas Huot-Marchand (ANRT) 

Based on Metafont, Metapost is an open source programming language which allowd to draw figures and by extension typefaces. In contrast to regular font drawing applications which model letters by their outlines, Metapost describes the letter by its "ductus", i.e. its central path or skeleton. Based on the letterer's gesture, this tool proposes a digital and parametric design of the character.


## Links and documentation
- Basic metapost instructions - <https://gitlab.com/erg-type/workshop.meta-elastique/-/blob/master/cheatsheet/cheatsheet.md>
- Un kit de survie - <https://gitlab.com/erg-type/metapost.survival-kit>
- Documentation metafont - <https://gitlab.com/speculoos-design/MetaFranquin/-/tree/master/doc/metafont>  
- Documentation metapost - <https://gitlab.com/speculoos-design/MetaFranquin/-/tree/master/doc/metapost>
- Metapost and dance! - <http://osp.kitchen/live/up-pen-down/#circle>


## Previous Workshops
- MetaHoguet <http://osp.kitchen/workshop/metahoguet/>
- Erg Caslon - <https://gitlab.com/erg-type/workshop.meta-elastique>