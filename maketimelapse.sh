for path in projects/*/snapshots;
do
  if [ -d $path ]; then
    previous="";
    echo "Making timelapse for: ${path}"
    if [ ! -d $path/png ]; then
      mkdir $path/png;
    fi
    rm $path/png/*.png;
    echo $path;
    for svg in $path/*.svg;
    do
      echo $svg;
      if [ ! $previous = "" ]; then
        diff=$(cmp $previous $svg);
      else
        diff="first";
      fi
      if [ ! "${diff}" = "" ]; then
        filename=$(basename $svg)
        convert $svg $path/png/${filename%.svg}.png
        previous=$svg;
      fi
    done
    ffmpeg -framerate 1 -r 3 -pattern_type glob -i "${path}/png/*.png" -c:v libx264 -pix_fmt yuv420p $path/timelapse.mp4
  fi
done;